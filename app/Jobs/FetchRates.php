<?php

namespace App\Jobs;

use Illuminate\Support\Facades\Redis;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Session;
use App\Services\Feeds;
use App\Services\Feeds\BlockchainFeed;
use App\Services\Feeds\CoindeskFeed;
use App\Services\Feeds\FixerFeed;
use App\Services\Feeds\CurrencyfeedFeed;

class FetchRates implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $sessionID;
    private $queueID;

    public function __construct($sessionID, $queueID)
    {
        $this->sessionID = $sessionID;
        $this->queueID = $queueID;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //check if the job should be executed
        $redisQueueID = Redis::get('fetching:' . $this->sessionID);
        if ($redisQueueID != $this->queueID) {
            //returning false here will break this queue job recursion and stop the queue branch from executing further
            return false;
        }

        //handle and get data
        $feedData = [
            'btcFeeds' => [
                'blockchain' => new BlockchainFeed(),
                'coindesk' => new CoindeskFeed(),
            ],
            'currencyFeeds' => [
                'fixer' => new FixerFeed(),
                'currencyfeed' => new CurrencyfeedFeed(),
            ]
        ];
        $feeds = new Feeds($feedData);

        $data = $feeds->getFeedsSummary();

        Redis::publish('feed', json_encode(['id' => $this->sessionID, 'event' => 'UpdateFeed', 'data' => $data]));

        //call this job recursively
        $job = (new FetchRates($this->sessionID, $this->queueID))->delay(\Carbon\Carbon::now()->addSeconds(1));
        dispatch($job);


    }
}
