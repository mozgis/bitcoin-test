<?php
namespace App\Services;

interface FetchRatesInterface
{

    /**
     * Fetches the feed data from given source, if no data is passed through constructor
     *
     * @return float
     */
    public function fetchData();

    /**
     * Gets the name of the feed
     *
     * @return string
     */
    public function getFeedName();

    /**
     * Checks, if feed instance was active and gave correct data after fetching it
     *
     * @return bool
     */
    public function isActive();
}
