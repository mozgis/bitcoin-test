<?php
namespace App\Services;

use Exception;

Class Feeds
{

    private $btcFeeds;
    private $currencyFeeds;

    private $activeFeeds = [
        'btc' => 0,
        'currency' => 0
    ];
    private $availableFeeds = [
        'btc' => 0,
        'currency' => 0
    ];

    public function __construct(array $feeds)
    {
        if (!is_array($feeds) || !isset($feeds['btcFeeds']) || !isset($feeds['currencyFeeds'])) {
            throw new Exception('Invalid feeds array passed');
        }
        //set up feeds
        $this->btcFeeds = $feeds['btcFeeds'];
        $this->currencyFeeds = $feeds['currencyFeeds'];
        $this->availableFeeds['btc'] = count($this->btcFeeds);
        $this->availableFeeds['currency'] = count($this->currencyFeeds);
    }

    /**
     * Gets best rate out of all BTC feeds provided in constructor
     *
     * @return array
     */
    public function getBestBtc():array
    {
        $output = ['bestFeed' => '', 'bestPrice' => null];
        foreach ($this->btcFeeds as $feed) {
            $price = $feed->fetchData();
            if ($feed->isActive()) {
                $this->activeFeeds['btc']++;
            }
            if (!is_float($output['bestPrice']) || floatval($output['bestPrice']) > $price) {
                $output = ['bestFeed' => $feed->getFeedName(), 'bestPrice' => $price];
            }
        }
        return $output;
    }


    /**
     * Gets best rate out of all currency feeds provided in constructor
     *
     * @return array
     */
    public function getBestCurrency():array
    {
        $output = ['bestFeed' => '', 'bestPrice' => null];
        foreach ($this->currencyFeeds as $feed) {
            $price = $feed->fetchData();
            if ($feed->isActive()) {
                $this->activeFeeds['currency']++;
            }
            if (!is_float($output['bestPrice']) || floatval($output['bestPrice']) > $price) {
                $output = ['bestFeed' => $feed->getFeedName(), 'bestPrice' => $price];
            }
        }
        return $output;
    }

    /**
     * Generates a array with feed price information from rates
     *
     * @return array
     */
    public function getFeedsSummary():array
    {
        $currFeedBest = $this->getBestCurrency();
        $btcFeedBest = $this->getBestBtc();

        $btcToEur = $btcFeedBest['bestPrice'] / $currFeedBest['bestPrice'];
        $output = [
            'BTCUSD' => $btcFeedBest['bestPrice'],
            'EURUSD' => $currFeedBest['bestPrice'],
            'BTCEUR' => $btcToEur,
            'activeBtc' => $this->activeFeeds['btc'],
            'availableBtc' => $this->availableFeeds['btc'],
            'activeCurrency' => $this->activeFeeds['currency'],
            'availableCurrency' => $this->availableFeeds['currency'],
        ];

        return $output;
    }

}
