<?php

namespace App\Services\Feeds;

use App\Services\FetchRatesInterface;
use Exception;

class BlockchainFeed implements FetchRatesInterface
{

    private $url = 'https://blockchain.info/ticker';

    protected $feedName = 'blockchain';
    private $data;
    private $timeout = 5;
    private $isActive = false;


    public function __construct($data = null)
    {
        $this->data = $data;
    }

    /**
     * Fetches the feed data from given source, if no data is passed through constructor
     *
     * @return float
     */
    public function fetchData(): float
    {
        // If data is not already passed (for example for testing), then fetch the data.
        if (!$this->data) {
            //get the data via curl
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $this->url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $this->timeout);

            $result = curl_exec($ch);
            curl_close($ch);
            if ($result) {
                $this->isActive = true;
            }
            $this->data = $result;
        } else {
            $this->isActive = true;
        }

        return $this->parseData();
    }

    /**
     * Parse the rate out of data feed
     *
     * @return float
     * @throws \Exception
     */
    private function parseData():float
    {
        $dataObject = json_decode($this->data);
        if ($dataObject && is_object($dataObject) && isset($dataObject->USD) && isset($dataObject->USD->last)) {
            return floatval($dataObject->USD->last);
        } else {
            throw new Exception('Invalid Feed data');
        }

    }

    /**
     * Gets the name of the feed
     *
     * @return string
     */
    public function getFeedName():string
    {
        return $this->feedName;
    }

    /**
     * Checks, if feed instance was active and gave correct data after fetching it
     *
     * @return bool
     */
    public function isActive():bool
    {
        return $this->isActive;
    }
}
