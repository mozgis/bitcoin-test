<?php

namespace App\Services\Feeds;

use App\Services\FetchRatesInterface;

class CoindeskFeed implements FetchRatesInterface
{

    private $url = 'http://api.coindesk.com/v1/bpi/currentprice.json';

    protected $feedName = 'coindesk';
    private $data;
    private $timeout = 5;
    private $isActive = false;

    public function __construct($data = null)
    {
        $this->data = $data;
    }

    /**
     * Fetches the feed data from given source, if no data is passed through constructor
     *
     * @return float
     */
    public function fetchData():float
    {
        // If data is not already passed (for example for testing), then fetch the data.
        if (!$this->data) {
            //get the data. This could be changed to use a package to get the content
            //get the data via curl
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $this->url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $this->timeout);

            $result = curl_exec($ch);
            curl_close($ch);
            if ($result) {
                $this->isActive = true;
            }
            $this->data = $result;
        } else {
            $this->isActive = true;
        }

        return $this->parseData();
    }

    /**
     * Parse the rate out of data feed
     *
     * @return float
     * @throws \Exception
     */
    private function parseData():float
    {

        $dataObject = json_decode($this->data);
        if ($dataObject && is_object($dataObject) && isset($dataObject->bpi) && isset($dataObject->bpi->USD) && isset($dataObject->bpi->USD->rate_float)) {

            return floatval($dataObject->bpi->USD->rate_float);
        } else {
            throw new \Exception('Invalid Feed data');
        }

    }

    /**
     * Gets the name of the feed
     *
     * @return string
     */
    public function getFeedName():string
    {
        return $this->feedName;
    }

    /**
     * Checks, if feed instance was active and gave correct data after fetching it
     *
     * @return bool
     */
    public function isActive():bool
    {
        return $this->isActive;
    }
}
