# BTC ticker test task

## Installation

1. Check out the source with `git clone`
2. run `composer install` in the root directory of project. Info about composer can be found here: https://getcomposer.org/
3. run `npm install` in the root directory of project. Info about npm: https://www.npmjs.com/
4. In the root directory create .env with this content:

```
APP_ENV=local
APP_KEY=base64:iu73pfnxPUPxVO/6P5PvbMPFJzAdSFiQkSnB3olbrS0=
APP_DEBUG=true
APP_LOG_LEVEL=debug
APP_URL=http://localhost
 
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=secret
 
BROADCAST_DRIVER=redis
CACHE_DRIVER=redis
SESSION_DRIVER=redis
QUEUE_DRIVER=redis
 
REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379
 
MAIL_DRIVER=smtp
MAIL_HOST=mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
 
PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=

```

## Dependencies
1. php 7.0 and all Laravel deps: https://laravel.com/docs/5.4/installation#server-requirements
2. npm, node.js: https://www.npmjs.com/, https://nodejs.org/en/
3. Redis: https://redis.io/


## Running the app
1. SSH into the server and `cd` into project root
2. Run `php artisan serve` to start Laravel http server
3. Run `php artisan queue:work --sleep=0` to start queue worker, that will fetch the rates.
4. Run `node socket.js` to start the nodejs server

The app will be available at http://localhost:8000/ 

## Testing
To run phpunit tests on a linux, from project root type `vendor/bin/phpunit`