<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CoindeskFeedTest extends TestCase
{

    public function testFetchDataWithFakeData()
    {
        $coindeskFakeData = '{"time":{"updated":"Mar 19, 2017 20:39:00 UTC","updatedISO":"2017-03-19T20:39:00+00:00","updateduk":"Mar 19, 2017 at 20:39 GMT"},"disclaimer":"This data was produced from the CoinDesk Bitcoin Price Index (USD). Non-USD currency data converted using hourly conversion rate from openexchangerates.org","bpi":{"USD":{"code":"USD","symbol":"&#36;","rate":"1,046.2150","description":"United States Dollar","rate_float":1046.215},"GBP":{"code":"GBP","symbol":"&pound;","rate":"843.7881","description":"British Pound Sterling","rate_float":843.7881},"EUR":{"code":"EUR","symbol":"&euro;","rate":"973.7228","description":"Euro","rate_float":973.7228}}}';
        $feed = new \App\Services\Feeds\CoindeskFeed($coindeskFakeData);
        $rate = $feed->fetchData();

        $this->assertEquals(1046.215, $rate);

    }

    /**
     * @expectedException Exception
     */
    public function testWithInvalidData()
    {
        $coindeskFakeData = '';
        $feed = new \App\Services\Feeds\CoindeskFeed($coindeskFakeData);
        $rate = $feed->fetchData();

        $coindeskFakeData = '{"time":{"updated":"Mar 19, 2017 20:39:00 UTC","updatedISO":"2017-03-19T20:39:00+00:00","updateduk":"Mar 19, 2017 at 20:39 GMT"},"disclaimer":"This data was produced from the CoinDesk Bitcoin Price Index (USD). Non-USD currency data converted using hourly conversion rate from openexchangerates.org","bpi_INVALID":{"USD":{"code":"USD","symbol":"&#36;","rate":"1,046.2150","description":"United States Dollar","rate_float":1046.215},"GBP":{"code":"GBP","symbol":"&pound;","rate":"843.7881","description":"British Pound Sterling","rate_float":843.7881},"EUR":{"code":"EUR","symbol":"&euro;","rate":"973.7228","description":"Euro","rate_float":973.7228}}}';
        $feed = new \App\Services\Feeds\CoindeskFeed($coindeskFakeData);
        $rate = $feed->fetchData();

        $coindeskFakeData = '{"time":{"updated":"Mar 19, 2017 20:39:00 UTC","updatedISO":"2017-03-19T20:39:00+00:00","updateduk":"Mar 19, 2017 at 20:39 GMT"},"disclaimer":"This data was produced from the CoinDesk Bitcoin Price Index (USD). Non-USD currency data converted using hourly conversion rate from openexchangerates.org","bpi":{"USD":{"code":"USD_INVALID","symbol":"&#36;","rate":"1,046.2150","description":"United States Dollar","rate_float":1046.215},"GBP":{"code":"GBP","symbol":"&pound;","rate":"843.7881","description":"British Pound Sterling","rate_float":843.7881},"EUR":{"code":"EUR","symbol":"&euro;","rate":"973.7228","description":"Euro","rate_float":973.7228}}}';
        $feed = new \App\Services\Feeds\CoindeskFeed($coindeskFakeData);
        $rate = $feed->fetchData();

        $coindeskFakeData = '{"time":{"updated":"Mar 19, 2017 20:39:00 UTC","updatedISO":"2017-03-19T20:39:00+00:00","updateduk":"Mar 19, 2017 at 20:39 GMT"},"disclaimer":"This data was produced from the CoinDesk Bitcoin Price Index (USD). Non-USD currency data converted using hourly conversion rate from openexchangerates.org","bpi":{"USD":{"code":"USD","symbol":"&#36;","rate":"1,046.2150","description":"United States Dollar","rate_float_INVALID":1046.215},"GBP":{"code":"GBP","symbol":"&pound;","rate":"843.7881","description":"British Pound Sterling","rate_float":843.7881},"EUR":{"code":"EUR","symbol":"&euro;","rate":"973.7228","description":"Euro","rate_float":973.7228}}}';
        $feed = new \App\Services\Feeds\CoindeskFeed($coindeskFakeData);
        $rate = $feed->fetchData();
    }

    public function testFetchDataFromSource()
    {
        $feed = new \App\Services\Feeds\CoindeskFeed();
        $rate = $feed->fetchData();
        $this->assertTrue(is_float($rate));
    }
}
