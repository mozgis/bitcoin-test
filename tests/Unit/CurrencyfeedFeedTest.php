<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Services\Feeds\CurrencyfeedFeed;

class CurrencyfeedFeedTest extends TestCase
{

    public function testFetchDataWithFakeData()
    {
        $currencyfeedFakeData = '{"feed":{"entry":[{},{},{"title":{"type":"text","$t":"USD"},"content":{"type":"text","$t":"_cokwr: 1.248125"}}]}}';
        $feed = new \App\Services\Feeds\CurrencyfeedFeed($currencyfeedFakeData);
        $rate = $feed->fetchData();

        $this->assertEquals(1.248125, $rate);

    }

    /**
     * @expectedException Exception
     */
    public function testWithInvalidData()
    {
        $currencyfeedFakeData = '';
        $feed = new \App\Services\Feeds\CurrencyfeedFeed($currencyfeedFakeData);
        $rate = $feed->fetchData();

        $currencyfeedFakeData = '{"feed_INVALID":{"entry":[{},{},{"title":{"type":"text","$t":"USD"},"content":{"type":"text","$t":"_cokwr: 1.248125"}}]}}';
        $feed = new \App\Services\Feeds\CurrencyfeedFeed($currencyfeedFakeData);
        $rate = $feed->fetchData();

        $currencyfeedFakeData = '{"feed":{"entry_INVALID":[{},{},{"title":{"type":"text","$t":"USD"},"content":{"type":"text","$t":"_cokwr: 1.248125"}}]}}';
        $feed = new \App\Services\Feeds\CurrencyfeedFeed($currencyfeedFakeData);
        $rate = $feed->fetchData();

        $currencyfeedFakeData = '{"feed":{"entry":[{},{"title":{"type":"text","$t":"USD_INVALID"},"content":{"type":"text","$t":"_cokwr: 1.248125"}}]}}';
        $feed = new \App\Services\Feeds\CurrencyfeedFeed($currencyfeedFakeData);
        $rate = $feed->fetchData();

        $currencyfeedFakeData = '{"feed":{"entry":[{},{},{"title":{"type":"text","$t":"USD_INVALID"},"content":{"type":"text","$t":"_cokwr: 1.248125"}}]}}';
        $feed = new \App\Services\Feeds\CurrencyfeedFeed($currencyfeedFakeData);
        $rate = $feed->fetchData();

        $currencyfeedFakeData = '{"feed":{"entry":[{},{},{"title":{"type":"text","$t":"USD"},"content":{"type":"text","$t_INVALID":"_cokwr: 1.248125"}}]}}';
        $feed = new \App\Services\Feeds\CurrencyfeedFeed($currencyfeedFakeData);
        $rate = $feed->fetchData();

    }

    public function testFetchDataFromSource()
    {
        $feed = new \App\Services\Feeds\CurrencyfeedFeed();
        $rate = $feed->fetchData();
        $this->assertTrue(is_float($rate));
    }
}
