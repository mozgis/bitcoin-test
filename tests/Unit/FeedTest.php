<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Services\Feeds;

class FeedTest extends TestCase
{

    /**
     * @expectedException Exception
     */
    public function testWrongFeedsArray()
    {
        $feedData = [];
        $feeds = new Feeds($feedData);

        $feedData = ['btcFeeds' => []];
        $feeds = new Feeds($feedData);

        $feedData = ['currencyFeeds' => []];
        $feeds = new Feeds($feedData);

    }

    public function testLoadBtcFeedsWithFakeData()
    {
        $blockchainFakeData = '{"USD" : {"15m" : 1046.68, "last" : 1046.68, "buy" : 1041, "sell" : 1048.04,  "symbol" : "$"}}';
        $coindeskFakeData = '{"time":{"updated":"Mar 19, 2017 20:39:00 UTC","updatedISO":"2017-03-19T20:39:00+00:00","updateduk":"Mar 19, 2017 at 20:39 GMT"},"disclaimer":"This data was produced from the CoinDesk Bitcoin Price Index (USD). Non-USD currency data converted using hourly conversion rate from openexchangerates.org","bpi":{"USD":{"code":"USD","symbol":"&#36;","rate":"1,046.2150","description":"United States Dollar","rate_float":1046.215},"GBP":{"code":"GBP","symbol":"&pound;","rate":"843.7881","description":"British Pound Sterling","rate_float":843.7881},"EUR":{"code":"EUR","symbol":"&euro;","rate":"973.7228","description":"Euro","rate_float":973.7228}}}';

        $feedData = [
            'btcFeeds' => [
                'blockchain' => new \App\Services\Feeds\BlockchainFeed($blockchainFakeData),
                'coindesk' => new \App\Services\Feeds\CoindeskFeed($coindeskFakeData),
            ],
            'currencyFeeds' => [

            ]
        ];
        $feeds = new Feeds($feedData);

        $btcFeedResults = $feeds->getBestBtc();
        $this->assertTrue(is_array($btcFeedResults));
        $this->assertEquals([
            'bestFeed' => 'coindesk',
            'bestPrice' => 1046.215
        ], $btcFeedResults);

    }

    public function testLoadBtcFeedWithRealData()
    {
        $feedData = [
            'btcFeeds' => [
                'blockchain' => new \App\Services\Feeds\BlockchainFeed(),
                'coindesk' => new \App\Services\Feeds\CoindeskFeed(),
            ],
            'currencyFeeds' => [

            ]
        ];
        $feeds = new Feeds($feedData);

        $btcFeedResults = $feeds->getBestBtc();
        $this->assertTrue(is_array($btcFeedResults));
        $this->assertTrue(is_string($btcFeedResults['bestFeed']));
        $this->assertTrue(is_float($btcFeedResults['bestPrice']));

    }

    public function testLoadCurrencyFeedsWithFakeData()
    {
        $fixerFakeData = '{"base":"EUR","date":"2017-03-21","rates":{"AUD":1.3969,"BGN":1.9558,"BRL":3.3175,"CAD":1.4349,"CHF":1.075,"CNY":7.4418,"CZK":27.021,"DKK":7.4353,"GBP":0.86753,"HKD":8.3886,"HRK":7.4078,"HUF":308.29,"IDR":14387.0,"ILS":3.9046,"INR":70.493,"JPY":121.58,"KRW":1208.0,"MXN":20.508,"MYR":4.7804,"NOK":9.1253,"NZD":1.5309,"PHP":54.075,"PLN":4.2628,"RON":4.5646,"RUB":61.811,"SEK":9.4923,"SGD":1.5088,"THB":37.434,"TRY":3.8972,"USD":1.0802,"ZAR":13.582}}';
        $currencyfeedFakeData = '{"feed":{"entry":[{},{},{"title":{"type":"text","$t":"USD"},"content":{"type":"text","$t":"_cokwr: 1.248125"}}]}}';

        $feedData = [
            'btcFeeds' => [
            ],
            'currencyFeeds' => [
                'fixer' => new \App\Services\Feeds\FixerFeed($fixerFakeData),
                'currencyfeed' => new \App\Services\Feeds\CurrencyfeedFeed($currencyfeedFakeData),
            ]
        ];
        $feeds = new Feeds($feedData);

        $currencyFeedResults = $feeds->getBestCurrency();
        $this->assertTrue(is_array($currencyFeedResults));
        $this->assertEquals([
            'bestFeed' => 'fixer',
            'bestPrice' => 1.0802
        ], $currencyFeedResults);

    }

    public function testLoadCurrencyFeedWithRealData()
    {
        $feedData = [
            'btcFeeds' => [
            ],
            'currencyFeeds' => [
                'fixer' => new \App\Services\Feeds\FixerFeed(),
                'currencyfeed' => new \App\Services\Feeds\CurrencyfeedFeed(),
            ]
        ];
        $feeds = new Feeds($feedData);

        $currencyFeedResults = $feeds->getBestCurrency();
        $this->assertTrue(is_array($currencyFeedResults));
        $this->assertTrue(is_string($currencyFeedResults['bestFeed']));
        $this->assertTrue(is_float($currencyFeedResults['bestPrice']));

    }


    public function testFeedSummaryWithFakeData()
    {
        $blockchainFakeData = '{"USD" : {"15m" : 1046.68, "last" : 1046.68, "buy" : 1041, "sell" : 1048.04,  "symbol" : "$"}}';
        $coindeskFakeData = '{"time":{"updated":"Mar 19, 2017 20:39:00 UTC","updatedISO":"2017-03-19T20:39:00+00:00","updateduk":"Mar 19, 2017 at 20:39 GMT"},"disclaimer":"This data was produced from the CoinDesk Bitcoin Price Index (USD). Non-USD currency data converted using hourly conversion rate from openexchangerates.org","bpi":{"USD":{"code":"USD","symbol":"&#36;","rate":"1,046.2150","description":"United States Dollar","rate_float":1046.215},"GBP":{"code":"GBP","symbol":"&pound;","rate":"843.7881","description":"British Pound Sterling","rate_float":843.7881},"EUR":{"code":"EUR","symbol":"&euro;","rate":"973.7228","description":"Euro","rate_float":973.7228}}}';
        $fixerFakeData = '{"base":"EUR","date":"2017-03-21","rates":{"AUD":1.3969,"BGN":1.9558,"BRL":3.3175,"CAD":1.4349,"CHF":1.075,"CNY":7.4418,"CZK":27.021,"DKK":7.4353,"GBP":0.86753,"HKD":8.3886,"HRK":7.4078,"HUF":308.29,"IDR":14387.0,"ILS":3.9046,"INR":70.493,"JPY":121.58,"KRW":1208.0,"MXN":20.508,"MYR":4.7804,"NOK":9.1253,"NZD":1.5309,"PHP":54.075,"PLN":4.2628,"RON":4.5646,"RUB":61.811,"SEK":9.4923,"SGD":1.5088,"THB":37.434,"TRY":3.8972,"USD":1.0802,"ZAR":13.582}}';
        $currencyfeedFakeData = '{"feed":{"entry":[{},{},{"title":{"type":"text","$t":"USD"},"content":{"type":"text","$t":"_cokwr: 1.248125"}}]}}';

        $feedData = [
            'btcFeeds' => [
                'blockchain' => new \App\Services\Feeds\BlockchainFeed($blockchainFakeData),
                'coindesk' => new \App\Services\Feeds\CoindeskFeed($coindeskFakeData),
            ],
            'currencyFeeds' => [
                'fixer' => new \App\Services\Feeds\FixerFeed($fixerFakeData),
                'currencyfeed' => new \App\Services\Feeds\CurrencyfeedFeed($currencyfeedFakeData),
            ]
        ];
        $feeds = new Feeds($feedData);

        $feedData = $feeds->getFeedsSummary();

        $expectedOutput = [
            'BTCUSD' => 1046.215,
            'EURUSD' => 1.0802,
            'BTCEUR' => 968.53823366043,
            'activeBtc' => 2,
            'availableBtc' => 2,
            'activeCurrency' => 2,
            'availableCurrency' => 2,
        ];
        $this->assertEquals($expectedOutput, $feedData);
    }
}
