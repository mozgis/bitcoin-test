<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Services\Feeds\BlockchainFeed;

class BlockchainFeedTest extends TestCase
{

    public function testFetchDataWithFakeData()
    {
        $blockchainFakeData = '{"USD" : {"15m" : 1046.68, "last" : 1046.68, "buy" : 1041, "sell" : 1048.04,  "symbol" : "$"}}';
        $feed = new \App\Services\Feeds\BlockchainFeed($blockchainFakeData);
        $rate = $feed->fetchData();

        $this->assertEquals(1046.68, $rate);

    }

    /**
     * @expectedException Exception
     */
    public function testWithInvalidData()
    {
        $blockchainFakeData = '';
        $feed = new \App\Services\Feeds\BlockchainFeed($blockchainFakeData);
        $rate = $feed->fetchData();

        $blockchainFakeData = '{"USD_INVALID" : {"15m" : 1046.68, "last" : 1046.68, "buy" : 1041, "sell" : 1048.04,  "symbol" : "$"}}';
        $feed = new \App\Services\Feeds\BlockchainFeed($blockchainFakeData);
        $rate = $feed->fetchData();

        $blockchainFakeData = '{"USD" : {"15m" : 1046.68, "last_INVALID" : 1046.68, "buy" : 1041, "sell" : 1048.04,  "symbol" : "$"}}';
        $feed = new \App\Services\Feeds\BlockchainFeed($blockchainFakeData);
        $rate = $feed->fetchData();

    }

    public function testFetchDataFromSource()
    {
        $feed = new \App\Services\Feeds\BlockchainFeed();
        $rate = $feed->fetchData();
        $this->assertTrue(is_float($rate));
    }
}
