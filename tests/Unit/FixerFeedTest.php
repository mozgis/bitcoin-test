<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Services\Feeds\FixerFeed;

class FixerFeedTest extends TestCase
{

    public function testFetchDataWithFakeData()
    {
        $fixerFakeData = '{"base":"EUR","date":"2017-03-21","rates":{"AUD":1.3969,"BGN":1.9558,"BRL":3.3175,"CAD":1.4349,"CHF":1.075,"CNY":7.4418,"CZK":27.021,"DKK":7.4353,"GBP":0.86753,"HKD":8.3886,"HRK":7.4078,"HUF":308.29,"IDR":14387.0,"ILS":3.9046,"INR":70.493,"JPY":121.58,"KRW":1208.0,"MXN":20.508,"MYR":4.7804,"NOK":9.1253,"NZD":1.5309,"PHP":54.075,"PLN":4.2628,"RON":4.5646,"RUB":61.811,"SEK":9.4923,"SGD":1.5088,"THB":37.434,"TRY":3.8972,"USD":1.0802,"ZAR":13.582}}';
        $feed = new \App\Services\Feeds\FixerFeed($fixerFakeData);
        $rate = $feed->fetchData();

        $this->assertEquals(1.0802, $rate);

    }

    /**
     * @expectedException Exception
     */
    public function testWithInvalidData()
    {
        $fixerFakeData = '';
        $feed = new \App\Services\Feeds\FixerFeed($fixerFakeData);
        $rate = $feed->fetchData();

        $fixerFakeData = '{"base":"EUR","date":"2017-03-21","rates_INVALID":{"AUD":1.3969,"BGN":1.9558,"BRL":3.3175,"CAD":1.4349,"CHF":1.075,"CNY":7.4418,"CZK":27.021,"DKK":7.4353,"GBP":0.86753,"HKD":8.3886,"HRK":7.4078,"HUF":308.29,"IDR":14387.0,"ILS":3.9046,"INR":70.493,"JPY":121.58,"KRW":1208.0,"MXN":20.508,"MYR":4.7804,"NOK":9.1253,"NZD":1.5309,"PHP":54.075,"PLN":4.2628,"RON":4.5646,"RUB":61.811,"SEK":9.4923,"SGD":1.5088,"THB":37.434,"TRY":3.8972,"USD":1.0802,"ZAR":13.582}}';
        $feed = new \App\Services\Feeds\FixerFeed($fixerFakeData);
        $rate = $feed->fetchData();

        $fixerFakeData = '{"base":"EUR","date":"2017-03-21","rates":{"AUD":1.3969,"BGN":1.9558,"BRL":3.3175,"CAD":1.4349,"CHF":1.075,"CNY":7.4418,"CZK":27.021,"DKK":7.4353,"GBP":0.86753,"HKD":8.3886,"HRK":7.4078,"HUF":308.29,"IDR":14387.0,"ILS":3.9046,"INR":70.493,"JPY":121.58,"KRW":1208.0,"MXN":20.508,"MYR":4.7804,"NOK":9.1253,"NZD":1.5309,"PHP":54.075,"PLN":4.2628,"RON":4.5646,"RUB":61.811,"SEK":9.4923,"SGD":1.5088,"THB":37.434,"TRY":3.8972,"USD_INVALID":1.0802,"ZAR":13.582}}';
        $feed = new \App\Services\Feeds\FixerFeed($fixerFakeData);
        $rate = $feed->fetchData();

    }

    public function testFetchDataFromSource()
    {
        $feed = new \App\Services\Feeds\FixerFeed();
        $rate = $feed->fetchData();
        $this->assertTrue(is_float($rate));
    }
}
