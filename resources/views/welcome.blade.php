<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">


        <title>BTC Test task</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->

    </head>
    <body>
        <div id="container">
          <div>
            BTC/USD: @{{ rates.BTCUSD }}   EUR/USD:  @{{ rates.EURUSD }}   BTC/EUR: @{{ rates.BTCEUR }} Active sources: BTC/USD (@{{ rates.activeBtc }} of @{{ rates.availableBtc }})  EUR/USD (@{{ rates.activeCurrency }} of @{{ rates.availableCurrency }})

            @{{ rates.btc_rate }}</div>
        </div>

        <script src="https://unpkg.com/vue"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.7.3/socket.io.js"></script>

        <script>
          let socket = io('http://127.0.0.1:3005');
          new Vue({
            el: '#container',
            data: {
              rates: {
                BTCUSD: 'Loading...',
                EURUSD: '',
                BTCEUR: '',
                activeBtc: '',
                availableBtc: '',
                activeCurrency: '',
                availableCurrency: '',
              }
            },

            mounted(){
              socket.emit('set_session', '{{ session()->getId() }}');
              socket.on('feed.{{ session()->getId() }}:UpdateFeed', function(data) {
                this.rates = data.data;
              }.bind(this));


            }
          });

        </script>
    </body>
</html>
