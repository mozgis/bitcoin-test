<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('start-conn/{id}', function ($id) {
    // generate random key for queue. When the key is removed or owerwritten,
    // it will kill the recursive queue process
    $queueId = str_random(16);
    Redis::set('fetching:' . $id, $queueId);
    // dispatch initial job
    dispatch(new App\Jobs\FetchRates($id, $queueId));

    return '{"ok": "' . $queueId . '"}';
});

Route::get('stop-conn/{id}', function ($id) {
    // stop queue jobs with this sessionID
    Redis::set('fetching:' . $id, false);
    return 'ok';
});
