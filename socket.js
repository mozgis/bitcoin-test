var http = require('http');
var server = http.Server();
var io = require('socket.io')(server);
var request = require("request");


var laravelServer = 'http://localhost:8000';

var Redis = require('ioredis');
var redis = new Redis();

//set up subscriber for redis
redis.subscribe('feed');
redis.on('message', (channel, message) => {
    message = JSON.parse(message);
    io.emit(`${channel}.${message.id}:${message.event}`, message);
})

//set up listener for presence
io.on('connection', function (socket) {
    var session_id = 0;

    //set session ID and start queue job in Laravel
    socket.on('set_session', (message) => {
        session_id = message;
        request({
            uri: laravelServer + '/start-conn/' + session_id,
            method: "GET"
        });
    });

    //on disconnect send request to Laravel to kill the queue job
    socket.on('disconnect', function () {
        request({
            uri: laravelServer + '/stop-conn/' + session_id,
            method: "GET"
        });
    });

});


server.listen(3005);
